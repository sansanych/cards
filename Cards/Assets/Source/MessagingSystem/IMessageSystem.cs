﻿namespace Assets.Source.MessagingSystem
{
	public interface IMessageSystem
	{
		void EmitMessage(int type, object data);
		void Subscribe(IMessageListener listener);
		void Unsubscribe(IMessageListener listener);
	}
}
