﻿using System.Collections.Generic;

namespace Assets.Source.MessagingSystem
{
	public interface IMessageListener
	{
		IEnumerable<int> ListeningTypes { get; }

		void OnMessage(IMessage message);
	}
}
