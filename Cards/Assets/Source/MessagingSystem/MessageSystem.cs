﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Source.MessagingSystem
{
	public partial class MessageSystem : IMessageSystem
	{
		private readonly Dictionary<int, List<IMessageListener>> _listenersDictionary = new Dictionary<int, List<IMessageListener>>();
		private readonly Message _message = new Message();

		public void EmitMessage(int type, object data )
		{
			using (_message)
			{
				_message.Type = type;
				_message.Data = data;

				List<IMessageListener> list;
				if (!_listenersDictionary.TryGetValue(type, out list))
					return;

				foreach (var item in list)
				{
					item.OnMessage(_message);
				}
			}
		}

		public void Subscribe(IMessageListener listener)
		{
			foreach (var type in listener.ListeningTypes)
			{
				List<IMessageListener> list;
				if (!_listenersDictionary.TryGetValue(type, out list))
				{
					list = new List<IMessageListener> { listener };
					_listenersDictionary.Add(type, list);
					continue;
				}

				if (list.Contains(listener))
				{
					Debug.LogWarning("Listenere " + listener + " already registered for listening messages with " + type + " type!");
					continue;
				}

				list.Add(listener);
			}
		}

		public void Unsubscribe(IMessageListener listener)
		{
			foreach (var type in listener.ListeningTypes)
			{
				List<IMessageListener> list;
				if (!_listenersDictionary.TryGetValue(type, out list))
				{
					Debug.LogWarning("No listeners are registered for listening " + type + " type messages!");
					continue;
				}


				if (!list.Remove(listener))
					Debug.LogWarning("Listener " + listener + " haven't been registered for listening " + type + " type messages");
			}
		}
	}
}
