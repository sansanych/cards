﻿using System;

namespace Assets.Source.MessagingSystem
{
	public partial class MessageSystem
	{
		public class Message : IMessage, IDisposable
		{
			public int Type { get; set; }
			public object Data { get; set; }

			public void Dispose()
			{
				Type = -1;
				Data = null;
			}
		}
	}
}
