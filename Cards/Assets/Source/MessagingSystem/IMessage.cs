﻿namespace Assets.Source.MessagingSystem
{
	public interface IMessage
	{
		int Type { get; }
		object Data { get; }
	}
}
